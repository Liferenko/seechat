import React, { Component } 	from 'react';
import './App.css';

// my API modules
import { connect, sendMsg } 	from './api';
import Header 			from './components/Header/Header';
import ChatHistory 		from './components/ChatHistory/ChatHistory';

class App extends Component {
	constructor(props) {
		super(props)
		connect()
	}
	componentDidMount() {
		connect((msg) => {
			console.log("New Message")
			this.setState(prevState => ({
				chatHistory: [...this.state.chatHistory, msg]
		}))
		console.log(this.state);
		});
	}
	send() {
		console.log("hello in console");
		sendMsg("Hello directly to the screen");
	}
	
	render() {
	    return (
	      <div className="App">
		<Header />
		    <ChatHistory chatHistory={this.state.chatHistory} />
		<content>
			<button onClick={this.send}>Click it and it goes...</button>    
		</content>    
	      </div>
	    );
	  }
	}

export default App;
