/*
 * Maintainer: Paul Liferenko <https://t.me/Liferenko>
 *
 * Description: Client-part of real-time chat. 
 
 * cb - This cb (aka callback) will be called whenever we receive our message.	
	
 */


var socket = new WebSocket('ws://localhost:8080/ws');

let connect = (cb) => {
	console.log("Connecting...")

	socket.onopen = () => {
		console.log("Succesfully Connected")
	}
	socket.onmessage = (msg) => {
		console.log(`You say :${msg}`)
		cb(msg)
	}
	socket.onclose = (event) => {
		console.log(`Socket closed connection: ${event}`)
	}
	socket.onerror = (error) => {
		console.log(`Socket error: ${error}`)
	}
};

let sendMsg = (msg) => {
	console.log(`sending message: ${msg}`)
	socket.send(msg)
}

export { connect, sendMsg }
